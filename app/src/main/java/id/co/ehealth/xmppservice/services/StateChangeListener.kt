package id.co.ehealth.xmppservice.services

import org.jivesoftware.smack.XMPPConnection

open class StateChangeListener {
    open fun newConnection(connection: XMPPConnection) {}
    open fun connected(connection: XMPPConnection) {}
    open fun disconnected(connection: XMPPConnection?) {}
    open fun connecting() {}
    open fun disconnecting(connection: XMPPConnection?) {}
    open fun waitingForNetwork() {}
    open fun waitingForRetry() {}
}
