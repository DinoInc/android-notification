package id.co.ehealth.xmppservice.services

import android.content.Context
import android.os.Handler
import android.util.Log
import id.co.ehealth.xmppservice.XMPPSettings
import id.co.ehealth.xmppservice.notification.XMPPPubSubHandler
import id.co.ehealth.xmppservice.utils.ConnectivityManagerUtil
import id.co.ehealth.xmppservice.utils.SingletonHolder
import org.jivesoftware.smack.AbstractXMPPConnection
import org.jivesoftware.smack.ConnectionListener
import org.jivesoftware.smack.XMPPConnection
import org.jivesoftware.smack.tcp.XMPPTCPConnection
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration
import org.jivesoftware.smackx.disco.ServiceDiscoveryManager
import org.jivesoftware.smackx.disco.packet.DiscoverInfo
import java.util.*
import kotlin.collections.HashSet


class XMPPTransport private constructor(private val applicationContext: Context) : ConnectionListener {

    override fun connected(connection: XMPPConnection?) {}
    override fun authenticated(connection: XMPPConnection?, resumed: Boolean) {}

    override fun connectionClosed() {
        newState(State.Disconnected)
    }

    override fun connectionClosedOnError(e: java.lang.Exception?) {
        newState(State.Disconnected)
    }

    private var connectionConfiguration: XMPPTCPConnectionConfiguration? = null
    private var reconnectHandler: Handler? = null
    private val stateChangeListeners = Collections.synchronizedSet(HashSet<StateChangeListener>())

    private var connection: AbstractXMPPConnection? = null
    private var currentState = State.Disconnected

    fun isConnected(): Boolean = currentState == State.Connected

    init {
        addListener(XMPPPingManager(this))
        addListener(XMPPPubSubHandler(applicationContext))
    }

    private val reconnectRunnable = Runnable {
        Log.d(TAG,"scheduleReconnect: calling tryToConnect")
        tryToConnect()
    }

    enum class State {
        Connected,
        Connecting,
        Disconnecting,
        Disconnected,
        WaitingForNetwork,
        WaitingForRetry
    }

    fun addListener(listener: StateChangeListener) {
        stateChangeListeners.add(listener)
    }

    fun removeListener(listener: StateChangeListener) {
        stateChangeListeners.remove(listener)
    }

    fun start() {}

    fun connect() {
        changeState(State.Connected)
    }

    fun disconnect() {
        changeState(State.Disconnected)
    }

    fun reconnect() {
        if (this.isConnected()) {
            disconnect()
        }
        if (!this.isConnected()) {
            connect()
        }
    }

    private fun scheduleReconnect() {
        newState(State.WaitingForRetry)
        if (reconnectHandler == null) {
            reconnectHandler = Handler()
        }
        reconnectHandler!!.removeCallbacks(reconnectRunnable)
        Log.d(TAG,"scheduleReconnect: scheduling reconnect in 10 seconds")
        reconnectHandler!!.postDelayed(reconnectRunnable, 10000)
    }

    /**
     * Notifies the StateChangeListeners about the new state and sets mState to
     * newState. Does not add a log message.
     *
     * @param newState
     * the reason for the new state (only used is newState is Disconnected)
     */
    private fun newState(newState: State) {
        when (newState) {
            State.Connected -> stateChangeListeners.forEach {it.connected(connection!!) }
            State.Disconnected -> stateChangeListeners.forEach { it.disconnected(connection) }
            State.Connecting -> stateChangeListeners.forEach { it.connecting() }
            State.Disconnecting -> stateChangeListeners.forEach { it.disconnecting(connection) }
            State.WaitingForNetwork -> stateChangeListeners.forEach { it.waitingForNetwork() }
            State.WaitingForRetry -> stateChangeListeners.forEach { it.waitingForRetry() }
        }
        currentState = newState
    }

    @Synchronized
    private fun changeState(desiredState: State) {
        Log.d(TAG,"changeState: mState=$currentState, desiredState=$desiredState")
        when (currentState) {
            State.Connecting, State.Connected -> when (desiredState) {
                State.Connected -> {
                }
                State.Disconnected -> {
                    disconnectConnection()
                }
                State.WaitingForNetwork -> {
                    disconnectConnection()
                    newState(State.WaitingForNetwork)
                }
                else -> {
                    throw IllegalStateException()
                }
            }
            State.Disconnected -> when (desiredState) {
                State.Disconnected -> {
                }
                State.Connected -> {
                    tryToConnect()
                }
                State.WaitingForNetwork -> {
                    newState(State.WaitingForNetwork)
                }
                else -> {
                    throw IllegalStateException()
                }
            }
            State.WaitingForNetwork -> when (desiredState) {
                State.WaitingForNetwork -> {
                }
                State.Connected -> {
                    tryToConnect()
                }
                State.Disconnected -> {
                    newState(State.Disconnected)
                }
                else -> {
                    throw IllegalStateException()
                }
            }
            State.WaitingForRetry -> when (desiredState) {
                State.WaitingForNetwork -> {
                    newState(State.WaitingForNetwork)
                }
                State.Connected -> {
                    tryToConnect()
                }
                State.Disconnected -> {
                    newState(State.Disconnected)
                    reconnectHandler!!.removeCallbacks(reconnectRunnable)
                }
                else -> {
                    throw IllegalStateException()
                }
            }
            else -> {
                throw IllegalStateException("changeState: Unkown state change combination. mState="
                        + currentState + ", desiredState=" + desiredState)
            }
        }
    }

    private fun isConnectionConfigurationChangedOrNull(): Boolean {
        val xmppSettings = XMPPSettings.getInstance(applicationContext)
        return connectionConfiguration == null ||
                connectionConfiguration != xmppSettings.getConnectionConfiguration()
    }

    @Synchronized
    private fun tryToConnect() {
        val failureReason = XMPPSettings.getInstance(applicationContext).checkIfReadyToConnect()
        if (failureReason != null) {
            Log.w(TAG,"tryToConnect: failureReason=$failureReason")
            return
        }

        if (this.isConnected()) {
            Log.d(TAG,"tryToConnect: already connected, nothing to do here")
            return
        }

        if (!ConnectivityManagerUtil.hasDataConnection(applicationContext)) {
            Log.d(TAG,"tryToConnect: no data connection available")
            newState(State.WaitingForNetwork)
            return
        }

        newState(State.Connecting)

        val xmppSettings = XMPPSettings.getInstance(applicationContext)
        val connection: AbstractXMPPConnection

        try {
            if (isConnectionConfigurationChangedOrNull()) {
                connectionConfiguration = xmppSettings.getConnectionConfiguration()
            }

            connection = when (this.connection) {
                null -> XMPPTCPConnection(connectionConfiguration)
                else -> this.connection!!
            }

        } catch (e: Exception) {

            val exceptionMessage = e.message
            // Schedule a reconnect on certain exception causes
            if ("DNS lookup failure" == exceptionMessage) {
                Log.w(TAG, "tryToConnect: connection configuration failed. " +
                        "Scheduling reconnect. exceptionMessage=$exceptionMessage")
                scheduleReconnect()
            } else {
                Log.e(TAG, "tryToConnect: connection configuration failed. " +
                        "New State: Disconnected", e)
                newState(State.Disconnected)
            }

            return
        }


        Log.d(TAG,"tryToConnect: connecting")
        try {
            connection.connect()
            connection.addConnectionListener(this)
        } catch (e: Exception) {
            Log.e(TAG, "tryToConnect: Exception from connect()", e)
            scheduleReconnect()
            return
        }

        Log.d(TAG,"XMPPTransport: connected.")

        if (!connection.isAuthenticated) {
            try {
                connection.login(xmppSettings.username, xmppSettings.password)
            } catch (e: Exception) {
                val exceptionMessage = e.message

                if ("No response from the server." == exceptionMessage) {
                    Log.w(TAG,"tryToConnect: login failed. Scheduling reconnect. " +
                            "exceptionMessage=$exceptionMessage")
                    scheduleReconnect()
                } else {
                    Log.e(TAG, "tryToConnect: login failed. New State: Disconnected", e)
                    newState(State.Disconnected)
                }
                return
            }
        }

        // Login Successful
        Log.d(TAG,"XMPPTransport: authenticated.\\o/")

        if (this.connection != connection) {
            this.connection = connection
            stateChangeListeners.forEach { it.newConnection(this.connection!!) }
        }

        newState(State.Connected)
    }

    @Synchronized
    private fun disconnectConnection() {
        if (connection != null) {
            if (connection!!.isConnected) {
                newState(State.Disconnecting)
                Log.d(TAG,"disconnectConnection: disconnect start")
                connection!!.disconnect()
                Log.d(TAG,"disconnectConnection: disconnect stop")
            }
        }
    }

    companion object: SingletonHolder<XMPPTransport, Context>(::XMPPTransport) {
        const val TAG = "XmppService"
        init {
            ServiceDiscoveryManager.setDefaultIdentity(DiscoverInfo.Identity("client",
                    "", "bot"))
        }
    }

}
