package id.co.ehealth.xmppservice

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import id.co.ehealth.xmppservice.utils.SingletonHolder
import org.jivesoftware.smack.ConnectionConfiguration
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration


class XMPPSettings(context: Context) {

    private val sharedPreferences: SharedPreferences =
            PreferenceManager.getDefaultSharedPreferences(context)

    private var configuration: XMPPTCPConnectionConfiguration? = null

    var host: String
        set(value) = sharedPreferences.edit().putString(HOST, value).apply()
        get(): String = sharedPreferences.getString(HOST, "")
    var xmppHost: String
        set(value) = sharedPreferences.edit().putString(XMPP_HOST, value).apply()
        get(): String = sharedPreferences.getString(XMPP_HOST, "")
    var port: Int
        set(value) = sharedPreferences.edit().putInt(PORT, value).apply()
        get(): Int = sharedPreferences.getInt(PORT, 5222)
    var username: String
        set(value) = sharedPreferences.edit().putString(USERNAME, value).apply()
        get(): String = sharedPreferences.getString(USERNAME, "")
    var password: String
        set(value) = sharedPreferences.edit().putString(PASSWORD, value).apply()
        get(): String = sharedPreferences.getString(PASSWORD, "")
    var compression: Boolean
        set(value) = sharedPreferences.edit().putBoolean(COMPRESSION, value).apply()
        get(): Boolean = sharedPreferences.getBoolean(COMPRESSION, false)
    var securityMode: String
        set(value) = sharedPreferences.edit().putString(SECURITY_MODE, value).apply()
        get(): String = sharedPreferences.getString(SECURITY_MODE, "opt")


    fun checkIfReadyToConnect(): String? {
        if (password.isEmpty()) return "Password not set or empty"
        if (username.isEmpty()) return "Username not set or empty"
        if (host.isEmpty()) return "XMPP Server Host not specified"
        if (xmppHost.isEmpty()) return "XMPP Server service name not specified"
        return null
    }

    fun getConnectionConfiguration(): XMPPTCPConnectionConfiguration {
        if (configuration == null) {

            val configurationBuilder = XMPPTCPConnectionConfiguration.builder()
                    .setHost(host)
                    .setPort(port)
                    .setXmppDomain(xmppHost)
                    .setUsernameAndPassword(username, password)
                    .setCompressionEnabled(compression)
                    .setSendPresence(false)


            configurationBuilder.setSecurityMode(when (securityMode) {
                "opt" -> ConnectionConfiguration.SecurityMode.ifpossible
                "req" -> ConnectionConfiguration.SecurityMode.required
                "dis" -> ConnectionConfiguration.SecurityMode.disabled
                else -> throw IllegalArgumentException("Unkown security mode: $securityMode")
            })


            configuration = configurationBuilder.build()

        }

        return configuration!!
    }

    companion object: SingletonHolder<XMPPSettings, Context>(::XMPPSettings) {
        private const val HOST = "server_host"
        private const val XMPP_HOST = "server_xmpp_host"
        private const val PORT = "server_port"
        private const val USERNAME = "xmpp_username"
        private const val PASSWORD = "xmpp_password"
        private const val COMPRESSION = "xmpp_compression"
        private const val SECURITY_MODE = "xmpp_security_mode"
    }
}