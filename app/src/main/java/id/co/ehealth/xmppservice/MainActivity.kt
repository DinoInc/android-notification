package id.co.ehealth.xmppservice

import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import id.co.ehealth.xmppservice.notification.XMPPPubSubHandler
import id.co.ehealth.xmppservice.services.NetworkConnectivityService
import id.co.ehealth.xmppservice.services.StateChangeListener
import id.co.ehealth.xmppservice.services.XMPPService
import id.co.ehealth.xmppservice.services.XMPPTransport


class MainActivity : AppCompatActivity() {

    lateinit var xmppTransport: XMPPTransport
    lateinit var xmppPubSubHandler: XMPPPubSubHandler


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val xmppSettings = XMPPSettings.getInstance(applicationContext)
        xmppSettings.host = "xmpp.ehealth.co.id"
        xmppSettings.xmppHost = "ehealth.co.id"
        xmppSettings.username = "admin"
        xmppSettings.password = "DinoNeedMagic2"
        xmppSettings.securityMode = "dis"

        xmppTransport = XMPPTransport.getInstance(applicationContext)

        val intent = Intent(applicationContext, XMPPService::class.java)
        intent.action = XMPPService.ACTION_START_SERVICE
        startService(intent)

        scheduleJob()

        setContentView(R.layout.activity_main)

    }

    private fun scheduleJob() {
        val myJob = JobInfo.Builder(0, ComponentName(this, NetworkConnectivityService::class.java))
                .setRequiresCharging(true)
                .setMinimumLatency(1000)
                .setOverrideDeadline(2000)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setPersisted(true)
                .build()

        val jobScheduler = getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        jobScheduler.schedule(myJob)
    }

}
