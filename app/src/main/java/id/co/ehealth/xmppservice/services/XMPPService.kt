package id.co.ehealth.xmppservice.services

import android.app.Service
import android.content.Intent
import android.os.*
import android.util.Log
import org.jivesoftware.smack.android.AndroidSmackInitializer


class XMPPService: Service() {

    companion object {
        const val TAG = "XMPPService"

        const val ACTION_START_SERVICE = "start_transport"
        const val ACTION_STOP_SERVICE = "stop_transport"
        const val ACTION_RESTART_SERVICE = "restart_transport"
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    private var isRunning = false
    @Volatile private var serviceLooper: Looper? = null
    @Volatile private var serviceHandler: ServiceHandler? = null

    private inner class ServiceHandler(looper: Looper) : Handler(looper) {
        override fun handleMessage(msg: Message) {
            onHandleMessage(msg.obj as String)
        }
    }

    private fun initializeHandler() {
        val thread = HandlerThread("IntentServiceHandler")
        thread.start()

        serviceLooper = thread.looper
        serviceHandler = ServiceHandler(serviceLooper!!)
    }

    override fun onCreate() {
        super.onCreate()
        initializeHandler()
        AndroidSmackInitializer.initialize(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val action = if (intent == null) ACTION_START_SERVICE else intent.action

        performInServiceHandler(action)

        return when (action) {
            ACTION_START_SERVICE -> Service.START_STICKY
            else -> Service.START_NOT_STICKY
        }
    }

    private fun performInServiceHandler(action: String) {
        val msg = serviceHandler!!.obtainMessage()
        msg.obj = action
        serviceHandler!!.sendMessage(msg)
    }

    fun onHandleMessage(action: String) {
        Log.d(TAG, "onHandleMessage: $action")

        when (action) {
            ACTION_START_SERVICE -> {
                XMPPTransport.getInstance(this).start()
            }
            ACTION_STOP_SERVICE -> {
                XMPPTransport.getInstance(this).disconnect(); stopSelf()
            }
            ACTION_RESTART_SERVICE  -> {
                XMPPTransport.getInstance(this).reconnect()
            }
            else -> {
                throw IllegalStateException("Unkown intent action: $action")
            }
        }

        if (isRunning && !XMPPTransport.getInstance(this).isConnected()) {
            Log.d(TAG,"onHandleMessage: service is running, but XMPPTransport is not connected, issueing connect()")
            XMPPTransport.getInstance(this).connect()
        }
    }

}
