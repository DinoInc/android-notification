package id.co.ehealth.xmppservice.utils

import android.content.Context
import android.net.ConnectivityManager

object ConnectivityManagerUtil {
    fun hasDataConnection(context: Context): Boolean {
        val connectivityManager = context
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = connectivityManager.activeNetworkInfo ?: return false
        return activeNetwork.isConnected
    }
}