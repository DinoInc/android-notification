package id.co.ehealth.xmppservice.services

import android.app.job.JobParameters
import android.app.job.JobService
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager

class NetworkConnectivityService : JobService() {

    private var receiver: NetworkConnectivityService.ConnectivityReceiver = ConnectivityReceiver()

    override fun onStartJob(params: JobParameters): Boolean {
        registerReceiver(receiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
        return true
    }

    override fun onStopJob(params: JobParameters): Boolean {
        unregisterReceiver(receiver)
        return true
    }

    fun onNetworkConnectionChanged(isConnected: Boolean) {
        if (isConnected) {
            val i = Intent(applicationContext, XMPPService::class.java)
            i.action = XMPPService.ACTION_RESTART_SERVICE
            applicationContext.startService(i)
        }
    }

    inner class ConnectivityReceiver internal constructor() : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            onNetworkConnectionChanged(isConnected())
        }

        private fun isConnected(): Boolean {
            val cm = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = cm.activeNetworkInfo
            return activeNetwork != null && activeNetwork.isConnected
        }
    }
}
