package id.co.ehealth.xmppservice.services

import org.jivesoftware.smack.XMPPConnection
import org.jivesoftware.smackx.ping.PingFailedListener
import org.jivesoftware.smackx.ping.PingManager

class XMPPPingManager(private val transport: XMPPTransport) : StateChangeListener(), PingFailedListener {

    override fun connected(connection: XMPPConnection) {
        PingManager.getInstanceFor(connection).registerPingFailedListener(this)
    }

    override fun disconnected(connection: XMPPConnection?) {
        if (connection !== null) {
            PingManager.getInstanceFor(connection).unregisterPingFailedListener(this)
        }
    }

    override fun pingFailed() {
        transport.reconnect()
    }

    companion object {
        val PING_INTERVAL_SECONDS = 60

        init {
            PingManager.setDefaultPingInterval(PING_INTERVAL_SECONDS)
        }
    }

}
