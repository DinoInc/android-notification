package id.co.ehealth.xmppservice.notification

import android.content.Context
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.util.Log
import id.co.ehealth.xmppservice.R
import id.co.ehealth.xmppservice.services.StateChangeListener
import org.jivesoftware.smack.XMPPConnection
import org.jivesoftware.smackx.pubsub.*
import org.jivesoftware.smackx.pubsub.listener.ItemEventListener
import java.security.SecureRandom

class XMPPPubSubHandler(val context: Context): StateChangeListener(), ItemEventListener<Item> {

    private var user: String? = null
    private var node: LeafNode? = null

    override fun connected(connection: XMPPConnection) {
        user = connection.user.toString()

        node = PubSubManager.getInstance(connection)
                .getOrCreateLeafNode("testNode")
        node!!.addItemEventListener(this)
        node!!.subscribe(user)
    }

    override fun handlePublishedItems(event: ItemPublishEvent<Item>?) {
        event?.items?.forEach {item ->
            Log.d("XMPPPubSubHandler", "$item")
            doSomething(item)
        }
    }

    override fun disconnecting(connection: XMPPConnection?) {
        node!!.unsubscribe(user)
        node!!.removeItemEventListener(this)
    }

    private fun doSomething(item: Item?) {

        if (item is PayloadItem<*>) {
            val payloadString = item.payload.toString()

            val mBuilder = NotificationCompat.Builder(context, CHANNEL_ID)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("New Push Notification")
                    .setContentText(payloadString)
                    .setGroupSummary(true)
                    .setGroup(GROUP_NOTIFICATION)


            with(NotificationManagerCompat.from(context)) {
                notify(SecureRandom().nextInt(), mBuilder.build())
            }
        }

    }

    companion object {
        const val CHANNEL_ID = "PubSub"
        const val GROUP_NOTIFICATION = "PubSubGroup"
    }

}